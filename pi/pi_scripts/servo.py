"""
Control a basic servo based on cli input
"""

import RPi.GPIO as GPIO
import time
import argparse


OFFSET_DUTY = 0.5  # define pulse offset of servo
SERVO_MIN_DUTY = 2.5 + OFFSET_DUTY  # define pulse duty cycle for minimum angle of servo
SERVO_MAX_DUTY = 12.5 + OFFSET_DUTY  # define pulse duty cycle for maximum angle of servo
servoPin = 12


def map(value, fromLow, fromHigh, toLow, toHigh):  # map a value from one range to another range
    return (toHigh - toLow) * (value - fromLow) / (fromHigh - fromLow) + toLow


def setup():
    global p
    GPIO.setmode(GPIO.BOARD)  # use PHYSICAL GPIO Numbering
    GPIO.setup(servoPin, GPIO.OUT)  # Set servoPin to OUTPUT mode
    GPIO.output(servoPin, GPIO.LOW)  # Make servoPin output LOW level

    p = GPIO.PWM(servoPin, 50)  # set frequency to 50Hz
    p.start(0)  # Set initial Duty Cycle to 0


def servoWrite(angle):  # make the servo rotate to specific angle, 0-180
    if (angle < 0):
        angle = 0
    elif (angle > 180):
        angle = 180
    p.ChangeDutyCycle(map(angle, 0, 180, SERVO_MIN_DUTY, SERVO_MAX_DUTY))  # map the angle to duty cycle and output it


def destroy():
    p.stop()
    GPIO.cleanup()


def move_servo(angle):
    setup()
    servoWrite(angle)
    print(f"Moving servo to {angle}° angle")
    time.sleep(1)
    destroy()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("angle", help="Angle to which to rotate the servo")
    args = parser.parse_args()
    move_servo(int(args.angle))


