from smart_home.models import Measurement
import time
import RPi.GPIO as GPIO
from datetime import datetime
from DHT import DHT

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker
import json

Base = declarative_base()


class TempHumidMeasure(Base):
    __tablename__ = 'measurement'
    id = Column(Integer, primary_key=True)
    time = Column(DateTime, index=True, default=datetime.utcnow)
    body = Column(String)
    sensor_id = Column(Integer)


engine = create_engine("postgresql://admin:test@192.168.0.242/smarthome")
Session = sessionmaker(bind=engine)



DHTPin = 11

def loop():
    session = Session()
    dht = DHT(DHTPin)  # create a DHT class object
    counts = 0  # Measurement counts
    while (True):
        counts += 1
        print("Measurement counts: ", counts)
        for i in range(0, 15):
            chk = dht.readDHT11()  # read DHT11 and get a return value. Then determine whether data read is normal according to the return value.
            if (
                    chk is dht.DHTLIB_OK):  # read DHT11 and get a return value. Then determine whether data read is normal according to the return value.
                print("DHT11,OK!")
                break
            time.sleep(0.1)
        message_body = {
            "temperature": dht.temperature,
            "humidity": dht.humidity
        }
        measurement = Measurement(body=json.dumps(message_body), sensor_id=2)
        session.add(measurement)
        session.commit()

        print("Humidity : %.2f, \t Temperature : %.2f \n" % (dht.humidity, dht.temperature))
        time.sleep(10)


if __name__ == '__main__':
    print('Program is starting ... ')
    try:
        loop()
    except KeyboardInterrupt:
        GPIO.cleanup()
        exit()