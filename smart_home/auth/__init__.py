import qrcode
import jwt
from smart_home import db
from smart_home.models import User
from datetime import datetime, timedelta
from smart_home.config import Config


def generate_qr_code(token=None):
    qr = qrcode.QRCode(
        version=1,
        box_size=10,
        border=5)
    if token:
        input_data = token
    else:
        input_data = jwt.encode({"username": "guest", "exp": datetime.now() + timedelta(days=1)},
                                key=Config["SECRET_KEY"]).decode("utf-8")
    qr.add_data(input_data)
    qr.make(fit=True)
    img = qr.make_image(fill='black', back_color='white')
    img.save('smart_home/static/qrcode_guest.png')
