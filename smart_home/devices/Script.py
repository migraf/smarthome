from smart_home.devices import BaseDevice, command
import subprocess
import psutil


class Script(BaseDevice):
    def __init__(self, device_id: int, name: str = None, interpreter: str = "python3", path: str = None):
        super().__init__(device_id, name)
        self.interpreter = interpreter
        self.path = path
        self.process = None

    @command()
    def run(self, *args, **kwargs):
        run_args = [self.interpreter, self.path]
        if args:
            run_args += args

        if kwargs:
            pass
        self.process = subprocess.Popen(args=run_args, stdout=subprocess.PIPE, shell=True)
        print(self.process.stdout.read())

    @command()
    def stop(self):
        self.process.terminate()

if __name__ == '__main__':
    script = Script(4, "test", "python3")
    script.run()




