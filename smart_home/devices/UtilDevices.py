from smart_home.device import BaseDevice, command
from datetime import datetime, timedelta
import inspect
import pika
import subprocess
import threading


class TimeDevice(BaseDevice):
    def __init__(self, device_id: int, name: str = None):
        super().__init__(device_id, name)

    @command()
    def alarm(self, time):
        pass

    @command()
    def timer(self, duration):
        pass


if __name__ == '__main__':
    td = TimeDevice(3, "time")
    print(td.get_commands())
