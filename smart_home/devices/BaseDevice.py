from abc import ABC, abstractmethod
from functools import wraps
import inspect
from smart_home.models import Device, Command, Light
from smart_home import db


def command(*args):
    """
    Decorator for a command function that logs its execution and arguments

    Args:
        *args ():

    Returns:

    """

    def log_command(fn):
        def wrapper(*args, **kwargs):
            print(args[0].__dict__)
            command = Command(
                name=fn.__qualname__,
                device_id=args[0].__dict__["device_id"],
                args=args[0].__dict__
            )
            db.session.add(command)
            db.session.commit()
            print("Args", args)
            print("KwArgs", kwargs)
            print('{}.{}'.format(fn.__module__, fn.__qualname__))
            return fn(*args, **kwargs)

        return wrapper

    return log_command


class BaseDevice(ABC):
    device_id: int
    name: str

    def __init__(self, device_id: int, name: str = None):
        self.device_id = device_id
        self.name = name

    def get_commands(self):
        """
        Returns a list of all available commands and their arguments for the current device

        Returns:

        """
        commands = []
        # TODO get args
        for member in inspect.getmembers(self):
            if str(member[1]).__contains__("command_decorator"):
                name = member[0]
                commands.append(name)
        return commands

    def register(self):
        """
        Register the device and its commands to the central service

        Returns:

        """


class Node(BaseDevice):
    """
    Device representing a computing node i.e. raspberry pi/arduino
    """
    def __init__(self, device_id: int, name: str = None):
        super().__init__(device_id, name)
        self.type = "node"


class LightDevice(BaseDevice):
    """
    Device representing a light source

    """
    def __init__(self, device_id: int, name: str = None, dim=False, color: dict = None):
        super().__init__(device_id, name)
        self.type = "light"
        self.dim = dim
        self.color = color
        self.active = False

    @command()
    def on(self, blink=False, color: str = None):
        print(f"Turning on: {self.name}")
        self.active = True

    @command()
    def off(self):
        print(f"Turning {self.name} off")
        self.active = False



if __name__ == '__main__':
    light = LightDevice(2, "test")
    print(light.get_commands())
