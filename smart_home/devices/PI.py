from .BaseDevice import BaseDevice, command
# import RPi.GPIO as GPIO
import threading


class PI(BaseDevice):
    def __init__(self, device_id: int, name: str = None):
        super().__init__(device_id, name)

    @command()
    def gpio_on(self, pin):
        print(f"Turning on pin: {pin}")
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.HIGH)
        GPIO.cleanup()

    @command()
    def gpio_off(self, pin):
        print(f"Turning off pin: {pin}")
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.LOW)
        GPIO.cleanup()

    @command()
    def gpio_input(self, pin):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pin, GPIO.IN)
        GPIO.cleanup()

    @command()
    def gpio_output(self, pin):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pin, GPIO.OUT)
        GPIO.cleanup()

    def get_commands(self):
        return super().get_commands()

    @property
    def pins(self):
        pins = [
            [{"pin": 1, "description": "+3,3 V"}, {"pin": 2, "description": "+5 V"}],
            [{"pin": 3, "description": "(SDA) GPIO 2"}, {"pin": 4, "description": "+5 V"}],
            [{"pin": 5, "description": "(SCL) GPIO 3"}, {"pin": 6, "description": "GND"}],
            [{"pin": 7, "description": "(GPCLK0) GPIO 4"}, {"pin": 8, "description": "GPIO 14 (TXD)"}],
            [{"pin": 9, "description": "GND"}, {"pin": 10, "description": "GPIO 15 (RXD)"}],
            [{"pin": 11, "description": "GPIO 17"}, {"pin": 12, "description": "GPIO 18"}],
            [{"pin": 13, "description": "GPIO 27"}, {"pin": 14, "description": "GND"}],
            [{"pin": 15, "description": "GPIO 22"}, {"pin": 16, "description": "GPIO 23"}],
            [{"pin": 17, "description": "+ 3,3 V"}, {"pin": 18, "description": "GPIO 24"}],
            [{"pin": 19, "description": "(MOSI) GPIO 10"}, {"pin": 20, "description": "GND"}],
            [{"pin": 21, "description": "(MISO) GPIO 9"}, {"pin": 22, "description": "GPIO 25"}],
            [{"pin": 23, "description": "(SCLK) GPIO 11"}, {"pin": 24, "description": "(CE0) GPIO 8"}],
            [{"pin": 25, "description": "GND"}, {"pin": 26, "description": "(CE1) GPIO 7"}],
            [{"pin": 27, "description": "(ID_SD) GPIO 0"}, {"pin": 28, "description": "(ID_SC) GPIO 1"}],
            [{"pin": 29, "description": "GPIO 5"}, {"pin": 30, "description": "GND"}],
            [{"pin": 31, "description": "GPIO 6"}, {"pin": 32, "description": "(PWN0) GPIO 12"}],
            [{"pin": 33, "description": "(PWM1) GPIO 13"}, {"pin": 34, "description": "GND"}],
            [{"pin": 35, "description": "(PCM_FS) GPIO 19"}, {"pin": 36, "description": "GPIO 16"}],
            [{"pin": 37, "description": "GPIO 26"}, {"pin": 38, "description": "(PCM_DIN) GPIO 20"}],
            [{"pin": 39, "description": "GND"}, {"pin": 40, "description": "(PCM_DOUT) GPIO 21"}],
        ]
        return pins
