from .BaseDevice import BaseDevice, command


class Servo(BaseDevice):
    def __init__(self, device_id: int, name: str = None):
        super().__init__(device_id, name)

