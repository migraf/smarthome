from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField, SelectField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from smart_home.models import User
from wtforms import validators


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Sign in")


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class TokenForm(FlaskForm):
    token = TextAreaField("token")
    submit = SubmitField("Generate token")
    guest_token = BooleanField("guest")


class AddDeviceForm(FlaskForm):
    device_name = StringField('name', validators=[validators.required(), validators.length(max=50)])
    device_type = SelectField(u'Device Type',
                              choices=[("undefined", "undefined"), ("node", "node"), ("actuator", "actuator"),
                                       ("light", "light"), ("custom", "custom")])
    address = StringField('address', validators=[validators.required(), validators.length(max=140)])
    submit = SubmitField("Add device")


class StartTaskForm(FlaskForm):
    device = StringField('device', validators=[validators.required(), validators.length(max=50)])
    command = StringField('command', validators=[validators.required(), validators.length(max=50)])
    args = TextAreaField('args', validators=[validators.optional(), validators.length(max=140)])
    queue = StringField('queue', validators=[validators.optional(), validators.length(max=50)])
    submit = SubmitField('Start')

class PinForm(FlaskForm):
    pass

class GPIOForm(FlaskForm):
    pass

