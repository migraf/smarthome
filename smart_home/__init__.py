import base64

from flask import Flask
from smart_home.config import Config
import jwt
from .extensions import db, login, migrate
from smart_home.models import User


def create_app():
    server = Flask(__name__, template_folder="templates", static_folder='static')
    server.config.from_object(Config)
    db.init_app(server)
    migrate.init_app(server, db)
    login.init_app(server)

    @login.request_loader
    def get_user_from_request(request):
        api_key = request.args.get('api_key')
        if api_key:
            try:
                jwt.decode(api_key)
                return User.query.get(1)
            except:
                return None
        api_key = request.headers.get('Authorization')
        if api_key:
            api_key = api_key.replace('Basic ', '', 1)
            try:
                api_key = base64.b64decode(api_key)
            except TypeError:
                pass
            user = User.query.get(1)
            if user:
                return user
        # TODO Remove this to add full authorization
        return User.query.get(1)


    with server.app_context():
        from smart_home import routes, models
        from smart_home.monitoring import init_dashboard

        server.register_blueprint(routes.main_bp)

        server = init_dashboard(server)
        return server

