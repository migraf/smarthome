from smart_home import models, db
from flask import render_template, redirect, url_for, flash, request, Blueprint
from .forms import LoginForm, RegistrationForm, TokenForm, AddDeviceForm, StartTaskForm
from flask_login import current_user, login_user, logout_user
from flask_login import login_required
from werkzeug.urls import url_parse
from smart_home.auth import generate_qr_code
import pika
import json

api_pb = Blueprint('api', __name__, static_folder="static", template_folder="templates", url_prefix="/api")


@api_pb.route('/light/<light_id>', methods=["GET", "POST", "PUT"])
@api_pb.route('/light', methods=["GET"])
@login_required
def light_api(light_id=None):
    # Get all registered lights
    if not light_id:
        lights = models.Light.query.all()
        print(lights)
    else:
        # TODO get information about individual lightbulbs
        if request.method == "GET":
            pass


