from smart_home import models, db
from flask import render_template, redirect, url_for, flash, request, Blueprint
from .forms import LoginForm, RegistrationForm, TokenForm, AddDeviceForm, StartTaskForm
from flask_login import current_user, login_user, logout_user
from flask_login import login_required
from werkzeug.urls import url_parse
from smart_home.auth import generate_qr_code
import pika
import json
from smart_home.devices.PI import PI
from flask import current_app

main_bp = Blueprint('main', __name__, static_folder="static", template_folder="templates")


@main_bp.route('/')
@main_bp.route('/index')
@login_required
def index():
    return render_template("index.html", title="Home", user="Admin")


@main_bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = models.User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for("main.login"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')
        return redirect(next_page)

    return render_template('login.html', title="Sign In", form=form)


@main_bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@main_bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = models.User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('main.login'))
    return render_template('register.html', title='Register', form=form)


@main_bp.route("/token", methods=['GET', 'POST'])
@login_required
def token():
    form = TokenForm()
    if form.validate_on_submit():
        token = current_user.generate_token(current_app.config["SECRET_KEY"])
        token = token.decode("utf-8")
        form.token = token
        qr_code = generate_qr_code(token)
        return render_template('app-token.html', title='Token', form=form, code=True)
    return render_template('app-token.html', title='Token', form=form)


@main_bp.route("/assistant", methods=['GET', 'POST'])
@login_required
def assistant():
    return render_template('assistant.html')


@main_bp.route('/user/<username>')
@login_required
def user(username):
    user = models.User.query.filter_by(username=username).first_or_404()
    posts = [
        {"author": user, "body": "Test Item #1"},
        {"author": user, "body": "Test Item #2"}
    ]
    return render_template('user.html', user=user, posts=posts)


@main_bp.route('/devices', methods=['GET', 'POST'])
@main_bp.route('/devices/<device_id>')
@login_required
def devices(device_id=None):
    form = AddDeviceForm()
    selected = device_id
    available_devices = models.Device.query.all()
    if form.validate_on_submit():
        device = models.Device(name=form.device_name.data, device_type=form.device_type.data, address=form.address.data)
        db.session.add(device)
        db.session.commit()
        available_devices = models.Device.query.all()
        form = AddDeviceForm()
        return render_template('devices.html', form=form, devices=available_devices)
    if device_id:
        print(device_id)
        return render_template('devices.html', form=form, devices=available_devices, selected=selected)

    return render_template('devices.html', form=form, devices=available_devices)


@main_bp.route('/pi', methods=['GET', 'POST'])
@login_required
def pi():
    pi = PI(device_id=1, name="main")

    return render_template('pi.html', pins=pi.pins)


@main_bp.route('/tasks', methods=['GET', 'POST'])
@login_required
def tasks():
    tasks = []
    form = StartTaskForm()
    if form.validate_on_submit():
        print("Adding task to queue")
        connection = pika.BlockingConnection()
        channel = connection.channel()
        if form.queue.data:
            channel.queue_declare(form.queue.data, durable=True)
        else:
            channel.queue_declare("tasks", durable=True)
        message = {
            "device": form.device.data,
            "command": form.command.data,
            "args": form.args.data
        }
        print(message)
        channel.basic_publish(exchange='',
                              routing_key='tasks',
                              body=json.dumps(message).encode("utf-8"))
        flash("Published Message")
        form = StartTaskForm()
        return render_template('tasks.html', form=form, tasks=tasks)

    return render_template('tasks.html', form=form, tasks=tasks)


@main_bp.route('/add_job/<cmd>')
@login_required
def add_job(cmd):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()
    channel.queue_declare(queue='task_queue', durable=True)
    channel.basic_publish(
        exchange='',
        routing_key='task_queue',
        body=cmd,
        properties=pika.BasicProperties(
            delivery_mode=2,  # make message persistent
        ))
    connection.close()
    return " [x] Sent: %s" % cmd
