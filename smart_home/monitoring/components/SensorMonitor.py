import plotly.graph_objects as go
import dash_core_components as dcc
import dash_html_components as html
from dash import Dash
from dash.dependencies import Input, Output
from dateutil.relativedelta import relativedelta

from ..Sensor import Sensor
import pandas as pd


def generate_monitor(app: Dash, name: str, sensor: Sensor):

    data: pd.DataFrame = sensor.get_measurements()
    fig = go.Figure()

    if type(data) == pd.Series:
        print("Series")
        fig.add_trace(go.Scatter(x=range(len(data)), y=data))
        layout = html.Div(
            children=[
                html.H2(name),
                dcc.Graph(
                    id=f"monitor-graph-{name}",
                    figure=fig
                )
            ]
        )

    elif type(data) == pd.DataFrame:
        x = data["time"]
        for col in data.columns:
            if col != "time":
                fig.add_trace(go.Scatter(
                    name=col,
                    x=x,
                    y=data[col])
                )

        # Generate for the slider
        marks = []

        layout = html.Div(children=[
            html.H2(name),
            dcc.Graph(
                id=f"monitor-graph-{name}",
                figure=fig
                ),
            dcc.RangeSlider(
                id="time-slider",
                min=data["time"].min(),
                max=data["time"].max(),
                value=[data["time"].min(), data["time"].max()]

            ),
            html.Div(id='output-container-range-slider')
            ],
            className=["container", "monitor-container"])

        @app.callback(
            Output('output-container-range-slider', 'children'),
            [Input('time-slider', 'value')])
        def update_output(value):
            return 'You have selected "{}"'.format(value)

    else:
        raise ValueError("Data from sensor needs to either one dimensional or dataframe containing "
                         "contain a time column")

    return layout
