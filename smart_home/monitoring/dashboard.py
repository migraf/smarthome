from dash import Dash
import dash_html_components as html
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import os
from .Sensor import TempHumidSensor
from .components.SensorMonitor import generate_monitor


def init_dashboard(server):
    """Create a Plotly Dash dashboard."""
    style_sheet = "./assets/style"

    app = Dash(
        __name__,
        server=server,
        routes_pathname_prefix='/monitoring/',
        external_stylesheets=[
            "./assets/style/"
        ]
    )
    sensor = TempHumidSensor(sensor_id=2, name="Room Temperature and Humidity")

    colors = {
        'background': '#111111',
        'text': '#7FDBFF'
    }


    # Create Dash Layout
    app.layout = html.Div(id="dashboard-container", children=[
        html.H1(
            children='Smart Home Monitoring',
        ),
        generate_monitor(app, "Temperature Monitor", sensor)

    ])
    init_callbacks(app)

    return app.server


def init_callbacks(dash_app):
    pass
