from smart_home.models import Measurement
from smart_home import db
import pandas as pd


def get_measurements(sensor_id: int):
    measurements = Measurement.query.filter_by(sensor_id=sensor_id)
    df = pd.read_sql(measurements.statement, measurements.session.bind)
    return df
