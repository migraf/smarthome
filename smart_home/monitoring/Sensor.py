from abc import ABC, abstractmethod
from smart_home.models import Measurement
import pandas as pd
import json


class Sensor(ABC):
    """
    Base Class for a monitoring unit

    """
    name: str
    sensor_id: int

    def __init__(self, sensor_id: int, name: str = None):
        self.sensor_id = sensor_id
        self.name = name

    def get_measurements(self):
        query = Measurement.query.filter_by(sensor_id=self.sensor_id)
        df = pd.read_sql(query.statement, query.session.bind)

        return self._parse_body(df)

    @abstractmethod
    def _parse_body(self, raw_df: pd.DataFrame):
        pass


class TempHumidSensor(Sensor):
    def __init__(self, sensor_id: int, name: str = None):
        super().__init__(sensor_id, name)

    def _parse_body(self, raw_df: pd.DataFrame):
        def parse(body):
            val_dict = json.loads(body)
            return pd.Series(val_dict)
        data = raw_df["body"].apply(parse)
        data["time"] = raw_df["time"]
        return data





