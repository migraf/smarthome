from smart_home.extensions import db
from datetime import datetime, timedelta
from flask_login import UserMixin
from smart_home import login
from werkzeug.security import generate_password_hash, check_password_hash
import jwt


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_token(self, key, admin=False):
        if not admin:
            encoded = jwt.encode({
                "id": self.id,
                "exp": datetime.utcnow() + timedelta(days=2)
            },
                key=key)
            return encoded
        else:
            return jwt.encode({"id": self.id}, key=key)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Device(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True)
    address = db.Column(db.String())
    device_type = db.Column(db.String(50), default="undefined")


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Post {}>'.format(self.body)


class Sensor(db.Model):
    """
    A registered sensor object, a sensor is a type of device
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True)
    last_active = db.Column(db.DateTime, nullable=True)
    description = db.Column(db.String(), nullable=True)
    address = db.Column(db.String(200), nullable=True)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))


class SensorEntry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    body = db.Column(db.String)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'))


class Measurement(db.Model):
    """
    A measurement coming from a registered sensor
    """
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    body = db.Column(db.String)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'))


class DeviceCommand(db.Model):
    """
    ORM Model representing a command associated with a device and its arguments
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    args = db.Column(db.JSON, nullable=True)


class Light(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    active = db.Column(db.Boolean, default=False)
    type = db.Column(db.String(50), nullable=True)


class Command(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    issued_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    args = db.Column(db.JSON, nullable=True)


class Recipe(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    content = db.Column(db.String(), nullable=True)
    link = db.Column(db.String(), nullable=True)


class Script(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    interpreter = db.Column(db.String())
    path = db.Column(db.String())
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
