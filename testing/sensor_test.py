from PiHome.src.sensor import NetworkSensor

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())
engine = create_engine(os.getenv("SQL_ALCHEMY_URI"))
Session = sessionmaker(bind=engine)


def main():
    session = Session()

    params = {
        "lat": "48.521637",
        "lon": "9.057645",
        "appid": os.getenv("OPENWEATHER_API_KEY"),
        "units": "metric"
    }
    weather_sensor = NetworkSensor(id=1,
                                   address="https://api.openweathermap.org/data/2.5/onecall",
                                   token=os.getenv("OPENWEATHER_API_KEY"),
                                   params=params)
    print(weather_sensor.measure(session=session))



if __name__ == '__main__':
    main()
