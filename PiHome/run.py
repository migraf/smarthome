from PiHome.app import app, db

# db.drop_all()
db.create_all()

app.run(host="localhost", port=5000, debug=True)
