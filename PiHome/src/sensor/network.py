import pandas as pd
import requests
import json

from .base import Sensor


class NetworkSensor(Sensor):
    def __init__(self, id=None, name=None, address=None, token: str = None, username: str = None, password: str = None,
                 params: dict = None):
        super().__init__(id, name, address)


        # TODO how to store credentials
        self.token = token
        self.username = username
        self.password = password
        self.params = params

    def _measure(self, params: dict = None):

        if params:
            r = requests.get(self.address, params=params)
        else:
            r = requests.get(self.address, params=self.params)

        r.raise_for_status()

        return json.dumps(r.json())



