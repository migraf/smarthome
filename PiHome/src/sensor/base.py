from ..device import Device
from ...app.models import Measurement
import pandas as pd
from typing import List
from datetime import datetime
from ...app.models import Sensor as DBSensor


class Sensor(Device):
    def __init__(self, id=None, name=None, address=None):
        super().__init__(id, name, address)
        # TODO Check if id exists

    def measure(self, session=None):
        data = self._measure()
        if session:
            measure = Measurement(
                sensor_id=self.id,
                body=data,
                time=datetime.now()
            )
            session.add(measure)
            session.commit()
        return data

    def get_measurements(self, n: int = 200, start=None, end=None) -> pd.DataFrame:
        pass

    def _measure(self) -> str:
        """
        Get a single measurement from the sensor and return a json formatted string containing the measurements

        """
        pass

    def _process_measurement_body(self, measurements: List[Measurement]) -> pd.DataFrame:
        measurements_list = [m.body for m in measurements]
        return measurements_list
