from typing import List

from ..device import Device


class Room:
    def __init__(self, id: int, devices: List[Device] = None):
        self.id = id
        self._devices = devices

    @property
    def devices(self) -> List[Device]:
        if self._devices:
            return self._devices

        return self._get_devices()

    def _get_devices(self) -> List[Device]:
        pass
