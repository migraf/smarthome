
class Device:
    id: int
    name: str
    address: str

    def __init__(self, id=None, name=None, address=None):
        self.id = id
        self.name = name
        self.address = address

