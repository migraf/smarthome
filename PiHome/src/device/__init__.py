from .base import Device
from enum import Enum


class DeviceTypes(Enum):
    undefined = 0
    node = 1
    actuator = 2
    sensor = 3
    light = 4

