from .base import Device
from typing import List

class Node(Device):
    devices = List[Device]

    def __init__(self, id=None, name=None, address=None):
        super().__init__(id, name, address)

