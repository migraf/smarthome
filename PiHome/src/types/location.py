from dataclasses import dataclass


@dataclass
class Location:
    address: str


@dataclass
class WebLocation(Location):
    port: int

