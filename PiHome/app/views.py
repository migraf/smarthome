from flask_appbuilder import BaseView, has_access, expose

from . import appbuilder
from .Dashboard import Dash_App2
from .fab_views import DeviceView, SensorView, RoomView

appbuilder.add_view(RoomView(), name="Rooms", category="Rooms")

appbuilder.add_view(DeviceView(), name="Devices", category="Devices")
appbuilder.add_view(SensorView(), name="Sensors", category="Devices")


class MyDashAppGraph(BaseView):
    route_base = "/"

    @has_access
    @expose("/dashboard_graph/")
    def methoddash(self):
        return self.render_template("dash.html", dash_url=Dash_App2.url_base, appbuilder=appbuilder)


appbuilder.add_view_no_menu(MyDashAppGraph())
appbuilder.add_link(
    "Dashboard Graph", href="/dashboard_graph/", icon="fa-list", category="Monitoring", category_icon="fa-list"
)

# appbuilder.add_view(HomeView(), name="Home", category="Home")
# appbuilder.indexview = HomeView()
# appbuilder.add_view_no_menu(HomeView(), endpoint="/home")
