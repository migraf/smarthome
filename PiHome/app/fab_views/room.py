from flask_appbuilder import BaseView, ModelView, has_access, expose
from flask_appbuilder.models.sqla.interface import SQLAInterface
from ..models import Room
from .device import DeviceView


class RoomView(ModelView):
    datamodel = SQLAInterface(Room)
    related_views = [DeviceView]