from flask_appbuilder.baseviews import BaseView
from flask_appbuilder import expose, has_access, ModelView
from ..models import Device, Sensor
from flask_appbuilder.models.sqla.interface import SQLAInterface
from .home import HomeView
from .room import RoomView
from .device import DeviceView






class SensorView(ModelView):
    datamodel = SQLAInterface(Sensor)

