from flask_appbuilder import IndexView, expose


class HomeView(IndexView):
    index_template = "home.html"
