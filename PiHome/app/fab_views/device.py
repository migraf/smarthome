from flask_appbuilder import ModelView
from flask_appbuilder.models.sqla.interface import SQLAInterface

from ..models import Device


class DeviceView(ModelView):
    datamodel = SQLAInterface(Device)
    list_columns = ["name", "room", "created_at", "device_type"]
