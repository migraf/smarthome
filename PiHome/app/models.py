from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, JSON, Boolean
from sqlalchemy.orm import relationship
from flask_appbuilder import Model
from datetime import datetime


class Device(Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(140))
    created_at = Column(DateTime, index=True, default=datetime.now())
    updated_at = Column(DateTime, nullable=True)
    address = Column(String())
    device_type = Column(String(50), default="undefined")
    # sensors = relationship('Sensor')
    room_id = Column(Integer, ForeignKey('room.id'), nullable=True)
    room = relationship("Room")


class Sensor(Model):
    """
    A registered sensor object, a sensor is a type of device
    """
    id = Column(Integer, primary_key=True)
    name = Column(String(140))
    created_at = Column(DateTime, index=True, default=datetime.now())
    updated_at = Column(DateTime, nullable=True)
    last_active = Column(DateTime, nullable=True)
    description = Column(String(), nullable=True)
    address = Column(String(200), nullable=True)
    device_id = Column(Integer, ForeignKey('device.id'))
    device = relationship("Device")
    measurements = relationship('Measurement')



class Measurement(Model):
    """
    A measurement coming from a registered sensor
    """
    id = Column(Integer, primary_key=True)
    time = Column(DateTime, index=True, default=datetime.now())
    body = Column(String)
    sensor_id = Column(Integer, ForeignKey('sensor.id'))
    sensor = relationship("Sensor")


class DeviceCommand(Model):
    """
    ORM Model representing a command associated with a device and its arguments
    """
    id = Column(Integer, primary_key=True)
    name = Column(String(140))
    created_at = Column(DateTime, index=True, default=datetime.now())
    device_id = Column(Integer, ForeignKey('device.id'))
    device = relationship("Device")
    args = Column(JSON, nullable=True)


class Light(Model):
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, index=True, default=datetime.now())
    updated_at = Column(DateTime, nullable=True)
    device_id = Column(Integer, ForeignKey('device.id'))
    device = relationship("Device")
    active = Column(Boolean, default=False)
    type = Column(String(50), nullable=True)


class Command(Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(140))
    device_id = Column(Integer, ForeignKey('device.id'))
    issued_at = Column(DateTime, index=True, default=datetime.now())
    args = Column(JSON, nullable=True)


class Recipe(Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(140))
    content = Column(String(), nullable=True)
    link = Column(String(), nullable=True)


class Script(Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(140))
    interpreter = Column(String())
    path = Column(String())
    device_id = Column(Integer, ForeignKey('device.id'))


class Room(Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(140))
    description = Column(String(500), nullable=True)
    floor = Column(Integer(), nullable=True)

